# Fork Benchmarks
This project contains various implementations of the Fork method, which splits a enumerable into two enumerables based on a predicate.

![Benchmark Results](images/benchmarks.png)