﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace Benchmarks;

public class PartitionVsFork
{
    public IEnumerable<int> nums { get; init; }
    private readonly Func<int, bool> isEven = (int x) => x % 2 == 0;
    public PartitionVsFork()
    {
        nums = Enumerable.Range(1, 1_000_000);
    }

    [Benchmark]
    public (IEnumerable<int>, IEnumerable<int>) Partition()
    {
        var (pass, fail) = nums.Parition(isEven);
        return (pass.ToList(), fail.ToList());
    }
    [Benchmark]
    public (IEnumerable<int>, IEnumerable<int>) Fork()
    {
        var (pass, fail) = nums.Fork(isEven);
        return (pass.ToList(), fail.ToList());
    }

    [Benchmark]
    public (IEnumerable<int>, IEnumerable<int>) TwoWhereFork()
    {
        var (pass, fail) = nums.TwoWhereFork(isEven);
        return (pass.ToList(), fail.ToList());
    }

    [Benchmark]
    public (IEnumerable<int>, IEnumerable<int>) ForEachFork()
    {
        var (pass, fail) = nums.ForEachFork(isEven);
        return (pass.ToList(), fail.ToList());
    }
}

public static class Program
{
    public static void Main(string[] args)
    {
        var summary = BenchmarkRunner.Run(typeof(Program).Assembly);
    }

    public static (IEnumerable<T> Passed, IEnumerable<T> Failed) Parition<T>
      (
         this IEnumerable<T> source,
         Func<T, bool> predicate
      )
    {
        var grouped = source.GroupBy(predicate);
        return
        (
           Passed: grouped.Where(g => g.Key).FirstOrDefault(Enumerable.Empty<T>()),
           Failed: grouped.Where(g => !g.Key).FirstOrDefault(Enumerable.Empty<T>())
        );
    }

    public static (IEnumerable<T> Passed, IEnumerable<T> Failed) Fork<T>
        (
            this IEnumerable<T> source,
            Func<T, bool> predicate
        )
    {
        var groupedByMatching = source.ToLookup(predicate);
        return (groupedByMatching[true], groupedByMatching[false]);
    }

    public static (IEnumerable<T> Passed, IEnumerable<T> Failed) TwoWhereFork<T>(this IEnumerable<T> ts, Func<T, bool> predicate)
        => (ts.Where(predicate), ts.Where(t => !predicate(t)));

    public static (IEnumerable<T> Passed, IEnumerable<T> Failed) ForEachFork<T>(this IEnumerable<T> ts, Func<T, bool> predicate)
    {
        var passed = new List<T>();
        var failed = new List<T>();

        foreach (var t in ts)
        {
            if (predicate(t))
                passed.Add(t);
            else
                failed.Add(t);
        }

        return (passed, failed);
    }

}
